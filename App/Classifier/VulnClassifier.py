import json
import time
from App.db import PurpleCVE, PurpleContentFiles
from App.config import db
from tqdm import tqdm
from tqdm.contrib.concurrent import thread_map
from App.Update.NvdLoader import NVDLoad
from App.Classifier.config import vulnerability_types


def load_cve_details_json():
    with open('/Users/mrunix/PycharmProjects/CveBasedContentSystem/App/Data/CveDetailsDump.json') as f:
        return json.load(f)


class AutomatedVulnerabilityClassifier:

    def __init__(self, vulnerability, db_insert=True):
        self.nvd_vulnerabilities = {}
        self.minimum_score = 8
        self.db_insert = db_insert
        self.vulnerability = vulnerability
        self.matched_vulnerabilities = {}
        self.unhandled_vulnerability = []

        self.cve_details_vulnerabilities = load_cve_details_json()
        self.nvd_files = [i.filename for i in PurpleContentFiles.query.all()]
        [self.nvd_vulnerabilities.update(NVDLoad('{0}'.format(filename)).cves) for filename in self.nvd_files]

    def insert_to_db(self, cve):
        # if self.matched_vulnerabilities.get(cve).get('classification') == self.vulnerability:
        if self.db_insert is False:
            return
        if PurpleCVE.query.filter_by(cve=cve).count() > 0:
            cve_object = PurpleCVE.query.filter_by(cve=cve).first()
            cve_object.Code_Execution = self.matched_vulnerabilities.get(cve).get('classification')
            db.session.commit()

    def automated_classification(self, cve):

        def _return_keywords_score(vulnerability_type, description, score=0):

            for key in vulnerability_types.get(vulnerability_type).get('keywords'):
                for item in vulnerability_types.get(vulnerability_type).get('keywords').get(key):

                    keyword, item_score = item

                    if key == 'include':
                        if keyword in description:
                            score += item_score
                        else:
                            continue
                    if key == 'exclude':
                        if keyword not in description:
                            continue
                        else:
                            score -= item_score

            return score

        def _return_vulnerability_location_value(locations, data_type):
            item = ''
            for location in locations.split('.'):
                try:
                    if data_type == 'cve_details':
                        item = item[location] if item else details_vulnerability[location]
                    else:
                        item = item[location] if item else nvd_vulnerability[location]
                except:
                    return None
            return item

        # for cve in list(self.cve_details_vulnerabilities.keys()):

        if PurpleCVE.query.filter_by(cve=cve).count() <= 0:
            return

        cve_score_classification = 0
        description_classification = {}
        nvd_vulnerability = self.nvd_vulnerabilities.get(cve)
        details_vulnerability = self.cve_details_vulnerabilities.get(cve)

        # Check if we got attributes of the vulnerability from 2 dbs
        if nvd_vulnerability is None or details_vulnerability is None:
            return

        for vulnerability_type in list(vulnerability_types.keys()):
            description_classification[vulnerability_type] = _return_keywords_score(vulnerability_type, nvd_vulnerability.get('description'))

        vulnerability_type = max(description_classification, key=description_classification.get)
        vulnerability_checks = vulnerability_types.get(vulnerability_type)

        if vulnerability_checks is None:
            self.unhandled_vulnerability.append(details_vulnerability)
            return

        for check in vulnerability_checks:
            for sub_check in vulnerability_checks.get(check):

                if check == 'keywords':
                    continue

                if check == 'nvd':
                    item = _return_vulnerability_location_value(sub_check, check)
                    if item is None:
                        continue
                    if item not in vulnerability_checks.get(check).get(sub_check):
                        continue
                    else:
                        cve_score_classification += 1

                if check == 'cve_details':
                    item = _return_vulnerability_location_value(sub_check, check)
                    if item is None:
                        continue

                    if type(vulnerability_checks.get(check).get(sub_check)) is list:
                        for i in vulnerability_checks.get(check).get(sub_check):
                            if i not in item:
                                cve_score_classification -= 4
                            else:
                                cve_score_classification += 4

        combined_score = description_classification.get(max(description_classification, key=description_classification.get)) + cve_score_classification
        if combined_score <= 5:
            self.unhandled_vulnerability.append((cve, combined_score))
            return

        self.matched_vulnerabilities[cve] = {
            'classification': max(description_classification, key=description_classification.get),
            'description_score': description_classification.get(max(description_classification, key=description_classification.get)),
            'attribute_score': cve_score_classification,
            'combined_score': combined_score,
            'vulnerability_details_classification': details_vulnerability.get('cvss').get('Vulnerability Type(s)'),
            'vulnerability_description': nvd_vulnerability.get('description'),
        }

        cve_list = list(self.matched_vulnerabilities.keys())
        # thread_map(self.insert_to_db, cve_list, max_workers=10)
        # print(self.matched_vulnerabilities)


t = time.process_time()
avc = AutomatedVulnerabilityClassifier('SCE')
print('Automated Classification')
thread_map(avc.automated_classification, avc.cve_details_vulnerabilities.keys(), max_workers=20)
print('Insert Results to DB')
thread_map(avc.insert_to_db, list(avc.matched_vulnerabilities.keys()),  max_workers=10)
# for cve in list(avc.matched_vulnerabilities.keys()):
#     avc.insert_to_db(cve=cve)

# thread_map(avc.insert_to_db, list(avc.matched_vulnerabilities.keys()))
elapsed_time = time.process_time() - t
print('Total Run Time: {0}'.format(elapsed_time))
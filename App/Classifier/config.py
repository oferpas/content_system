vulnerability_types = {
            'WEB': {
                'cve_details': {
                    'cvss.Vulnerability Type(s)': ['Cross Site Scripting',
                                                   'Directory traversal',
                                                   'Sql Injection',
                                                   'Http response splitting',
                                                   'CSRF'],
                },
                'nvd': {
                    'metricV2.cvssV2.accessVector': ['NETWORK', 'LOCAL'],
                    'metricV2.userInteractionRequired': [False],
                },
                'keywords': {
                    'include': [('cross-site scripting', 9), ('XSS', 9),
                                ('sql injection', 9), ('Sql Injection', 9),
                                ('Directory traversal', 9), ('directory traversal', 9),
                                ('response splitting', 9), ('CSRF', 9), ('cgi-bin', 9)],
                    'exclude': [('User interaction is required', 8), ('User interaction is needed', 8),
                                ('user-assisted attackers', 8), ('must visit', 8), ('malicious page', 8),
                                ('malicious file', 7), ('malicious url', 8), ('malicious web page', 8)],
                }
            },
            'DOS': {
                'cve_details': {
                    'cvss.Vulnerability Type(s)': ['Denial Of Service'],
                    'cvss.Authentication': ['Not required']
                },
                'nvd': {
                    'metricV2.cvssV2.authentication': ['NONE'],
                    'metricV2.cvssV2.availabilityImpact': ['COMPLETE', 'PARTIAL'],
                    'metricV2.cvssV2.confidentialityImpact': ['NONE'],
                    'metricV2.userInteractionRequired': [False]
                },
                'keywords': {
                    'include': [('crash', 5), ('denial of service', 5), ('dos', 5)],
                    'exclude': [('remote code', 4), ('gain privileges', 4), ('privileges', 4)],
                }
            },
            'PRIVESC': {
                'cve_details': {
                    'cvss.Vulnerability Type(s)': ['Gain privileges']
                },
                'nvd': {
                    'metricV2.cvssV2.accessVector': ['LOCAL'],
                    'metricV2.userInteractionRequired': [False]
                },
                'keywords': {
                    'include': [('root privileges', 7), ('gain privileges', 6), ('gain user privileges', 8),
                                ('local user gains', 8), ('gain root privileges', 8)],
                    'exclude': [('remote attackers', 3), ('remote code', 3), ('crash', 3)],
                }
            },
            'SCE': {
                'cve_details': {
                    'cvss.Vulnerability Type(s)': ['Execute Code', 'Overflow'],
                    'cvss.Authentication': ['Not required'],
                    'metricV2.userInteractionRequired': [True, False]
                },
                'nvd': {
                    'metricV2.cvssV2.accessVector': ['NETWORK', 'LOCAL'],
                    'metricV2.cvssV2.userInteractionRequired': [True],
                },
                'keywords': {
                    'include': [('User interaction is required', 8), ('User interaction is needed', 8), ('Outlook', 6),
                                ('user-assisted attackers', 8), ('user-assisted remote attackers', 8), ('ActiveX', 7),
                                ('malicious Java applet', 6), ('malicious XLM', 6),('via a crafted Web page', 6),
                                ('Macro names', 6), ('malicious document', 6), ('malformed PNG', 6), ('via a link', 6),
                                ('must visit', 6), ('malicious page', 6), ('malicious file', 6), ('malicious url', 6),
                                ('malformed IMDATA', 7), ('Internet Explorer', 10), ('Microsoft Excel', 10), ('Microsoft Graphics Components', 10),
                                ('Microsoft Edge', 10), ('trick the victim', 8), ('Google Chrome', 10),
                                ('in WinZip', 8), ('malicious web page', 6), ('malformed GIF', 5), ('tricking a user', 10),
                                ('via a GIF', 9), ('via a crafted document', 5), ('via a javascript', 5), ('Office', 5),
                                ('SeaMonkey', 9), ('Thunderbird', 6), ('Firefox', 9), ('SVG file', 6), ('via a playlist', 6),
                                ('Firescrolling', 5), ('Firelinking', 5), ('Firesearching', 5), ('using an IFRAME', 6),
                                ('tricking the user', 8), ('firefox', 9), ('using an XHTML', 5), ('via an XBM', 5)],
                    'exclude': [('crash', 5), ('local users', 3), ('gain privileges', 5), ('gain root privileges', 5)]
                }
            },

            'RCE': {
                'cve_details': {
                    'cvss.Vulnerability Type(s)': ['Execute Code', 'Overflow'],
                },
                'nvd': {
                    'metricV2.cvssV2.accessVector': ['NETWORK'],
                    'metricV2.cvssV2.accessComplexity': ['LOW', 'MEDIUM'],
                    'metricV2.cvssV2.confidentialityImpact': ['COMPLETE', 'PARTIAL'],
                    'metricV2.cvssV2.userInteractionRequired': [False],
                    'metricV2.obtainAllPrivilege': [True, False],
                },
                'keywords': {
                    'include': [('access to remote attackers', 5), ('remote attackers', 2), ('remote code', 4),
                                ('via buffer', 3), ('buffer overflow', 3), ('packet', 7), ('network', 7), ('In IIS', 7),
                                ('in IIS', 7), ('IIS 5.0 ', 7)],
                    'exclude': [('denial of service', 6), ('dos', 6), ('crash', 6), ('Firefox', 6),
                                ('local users', 6), ('user-assisted attackers', 6), ('SeaMonkey', 6), ('Thunderbird', 6),
                                ('User interaction', 4), ('must visit', 4), ('malicious page', 4), ('malicious file', 4),
                                ('malicious url', 5), ('malicious web page', 5), ('backdoor', 5), ('Office', 5), ('malformed IMDATA', 5)],
                }
            }
        }
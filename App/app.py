import csv
from flask import Flask, redirect, jsonify, send_file, make_response, Response
from App.config import admin, app, db
from App.db import PurpleCVE, PurpleCveToKb, PurpleContentFiles, PurpleApplication, PurpleEOL
from App.views import AdminView, FilesView, KbView, ApplicationView, EOLView
from flask_executor import Executor
from App.Update.Updater import Updater

executor = Executor(app)

@app.route('/')
def index():
    return redirect("/admin/purplecve", code=302)


@app.route('/api/v1/update-cvedetailes')
def update_cve_details():
    executor.submit(Updater().get_cve_details_files)
    return make_response(200)


@app.route('/api/v1/updatedb')
def update_database():
    executor.submit(Updater().RunUpdate)
    return Response(status=200)
    # return make_response(200)


@app.route('/api/v1/EOL')
def api_end_of_life():
    data = []
    for item in PurpleEOL.query.all():
        data.append({key: value for key, value in item.__dict__.items() if '_sa' not in key and 'id' not in key})
    return jsonify(data)


@app.route('/api/v1/applications')
def api_applications():
    data = []
    for item in PurpleApplication.query.all():
        data.append({key: value for key, value in item.__dict__.items() if '_sa' not in key and 'id' not in key})
    return jsonify(data)


@app.route('/api/v1/execution')
def api_code_execution_all():
    classification = {'RCE': {}, 'LCE': {}, 'SCE': {}, 'DOS': {}, 'PRIVESC': {}}
    data = PurpleCVE.query.filter(PurpleCVE.Code_Execution.isnot(None)).all()
    for item in data:
        classification[item.Code_Execution][item.cve] = {key: value for key, value in item.__dict__.items() if '_sa' not in key and 'id' not in key and 'cve' not in key}
    
    return jsonify(classification)

# @app.route('/api/v1/execution/ml')
# def api_for_ml():
#     classification = []
#     csv_file = "data_for_ml.csv"
#     csv_columns = ['CVE', 'Description', 'Code_Execution']
#     data = CVE.query.filter(CVE.Code_Execution.isnot(None)).all()
#     for item in data:
#         classification.append(
#             {'CVE': item.cve, 'Description': item.description, 'Code_Execution': item.Code_Execution})
#
#     with open(csv_file, 'w') as csvfile:
#         writer = csv.DictWriter(csvfile, fieldnames=csv_columns, delimiter=';')
#         writer.writeheader()
#         for data in classification:
#             writer.writerow(data)
#     return send_file(csv_file, as_attachment=True)


@app.route('/api/v1/kb')
def cve_to_kb():
    data = []
    for item in PurpleCveToKb.query.all():
        data.append({key: value for key, value in item.__dict__.items() if '_sa' not in key and 'id' not in key})
    return jsonify(data)


@app.route('/api/v1/db')
def classification_db():
    data = []
    for item in PurpleCVE.query.all():
        data.append({key: value for key, value in item.__dict__.items() if '_sa' not in key and 'id' not in key})
    return jsonify(data)

db.create_all()
admin.add_view(AdminView(PurpleCVE, db.session, 'CVE'))
admin.add_view(FilesView(PurpleContentFiles, db.session, 'Content Files'))
admin.add_view(KbView(PurpleCveToKb, db.session, 'Microsoft KB'))
admin.add_view(ApplicationView(PurpleApplication, db.session, 'Application Definitions'))
admin.add_view(EOLView(PurpleEOL, db.session, 'OS EOL'))


if __name__ == '__main__':
    # db.create_all()
    # db.migrate()
    app.run(debug=False, host='0.0.0.0', port=8080)
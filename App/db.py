from App.config import db


class UpdateProcess(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    status = db.Column(db.String(250))
    date = db.Column(db.String(250))


class PurpleContentFiles(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    filename = db.Column(db.String(250))
    http_status = db.Column(db.String(250))
    file_status = db.Column(db.String(250))
    file_content = db.Column(db.Text())
    MD5 = db.Column(db.String(250))
    date = db.Column(db.String(250))


class PurpleCPE(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    Product_Name = db.Column(db.String(250))
    Product_CPE = db.Column(db.String(250))
    Platform_CPE = db.Column(db.String(250))


class PurpleEOL(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(300))
    cycle = db.Column(db.String(250))
    cycleShortHand = db.Column(db.String(250))
    lts = db.Column(db.Boolean())
    release = db.Column(db.String(250))
    support = db.Column(db.String(250))
    eol = db.Column(db.String(250))
    latest = db.Column(db.String(250))
    link = db.Column(db.String(250))
    MD5 = db.Column(db.String(250))


class PurpleApplication(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    Application_Name = db.Column(db.String(250))
    Application_Service = db.Column(db.String(250))
    TCP = db.Column(db.String(1000))
    UDP = db.Column(db.String(1000))


class PurpleCveToKb(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    cve = db.Column(db.String(20))
    description = db.Column(db.Text())
    ExploitStatus = db.Column(db.Text())
    FullProductName = db.Column(db.String(250))
    RemediationType = db.Column(db.String(250))
    RestartRequired = db.Column(db.String(250))
    ProductId = db.Column(db.String(250))
    Supercedence = db.Column(db.Text())
    UrlBinaries = db.Column(db.String(1000))
    UrlFix = db.Column(db.String(250))
    VendorFix = db.Column(db.String(250))


class PurpleCVE(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    cve = db.Column(db.String(50), unique=True, nullable=False)
    cwe = db.Column(db.String(50))
    title = db.Column(db.Text())

    references = db.Column(db.Text())
    description = db.Column(db.Text())
    published_date = db.Column(db.String(100))
    last_modified_date = db.Column(db.String(100))

    metricv2_cvss_version = db.Column(db.String(10))
    metricv2_cvss_vectorString = db.Column(db.String(50))
    metricv2_cvss_accessVector = db.Column(db.String(50))
    metricv2_cvss_accessComplexity = db.Column(db.String(50))
    metricv2_cvss_authentication = db.Column(db.String(50))
    metricv2_cvss_confidentialityImpact = db.Column(db.String(50))
    metricv2_cvss_integrityImpact = db.Column(db.String(50))
    metricv2_cvss_availabilityImpact = db.Column(db.String(50))
    metricv2_cvss_baseScore = db.Column(db.Float())

    metricv2_severity = db.Column(db.String(80))
    metricv2_exploitability_score = db.Column(db.Float())
    metricv2_impact_score = db.Column(db.Float())
    metricv2_acInsufInfo = db.Column(db.Boolean())
    metricv2_obtainAllPrivilege = db.Column(db.Boolean())
    metricv2_obtainUserPrivilege = db.Column(db.Boolean())
    metricv2_obtainOtherPrivilege = db.Column(db.Boolean())
    metricv2_userInteractionRequired = db.Column(db.Boolean())

    metricv3_cvss_version = db.Column(db.String(10))
    metricv3_cvss_vectorString = db.Column(db.String(50))
    metricv3_cvss_attackVector = db.Column(db.String(50))
    metricv3_cvss_attackComplexity = db.Column(db.String(50))
    metricv3_cvss_privilegesRequired = db.Column(db.String(50))
    metricv3_cvss_userInteraction = db.Column(db.String(50))
    metricv3_cvss_scope = db.Column(db.String(50))
    metricv3_cvss_confidentialityImpact = db.Column(db.String(50))
    metricv3_cvss_integrityImpact = db.Column(db.String(50))
    metricv3_cvss_availabilityImpact = db.Column(db.String(50))
    metricv3_cvss_baseScore = db.Column(db.Float())
    metricv3_cvss_baseSeverity = db.Column(db.String(50))

    metricv3_exploitability_score = db.Column(db.Float())
    metricv3_impact_score = db.Column(db.Float())

    Code_Execution = db.Column(db.String(10))
    Product_CPE = db.Column(db.Text())
    Platform_CPE = db.Column(db.Text())
    Network_Service = db.Column(db.String(20))
    Network_Protocol = db.Column(db.String(10))
    Network_Port = db.Column(db.String(50))
    From_Patch = db.Column(db.Boolean())
    MD5 = db.Column(db.String(32))


class CVE(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    cve = db.Column(db.String(50), unique=True, nullable=False)
    cwe = db.Column(db.String(50))
    references = db.Column(db.Text())
    description = db.Column(db.Text())

    metricv2_cvss_version = db.Column(db.String(10))
    metricv2_cvss_vectorString = db.Column(db.String(50))
    metricv2_cvss_accessVector = db.Column(db.String(50))
    metricv2_cvss_accessComplexity = db.Column(db.String(50))
    metricv2_cvss_authentication = db.Column(db.String(50))
    metricv2_cvss_confidentialityImpact = db.Column(db.String(50))
    metricv2_cvss_integrityImpact = db.Column(db.String(50))
    metricv2_cvss_availabilityImpact = db.Column(db.String(50))
    metricv2_cvss_baseScore = db.Column(db.Float())

    metricv2_severity = db.Column(db.String(80))
    metricv2_exploitability_score = db.Column(db.Float())
    metricv2_impact_score = db.Column(db.Float())
    metricv2_acInsufInfo = db.Column(db.Boolean())
    metricv2_obtainAllPrivilege = db.Column(db.Boolean())
    metricv2_obtainUserPrivilege = db.Column(db.Boolean())
    metricv2_obtainOtherPrivilege = db.Column(db.Boolean())
    metricv2_userInteractionRequired = db.Column(db.Boolean())

    metricv3_cvss_version = db.Column(db.String(10))
    metricv3_cvss_vectorString = db.Column(db.String(50))
    metricv3_cvss_attackVector = db.Column(db.String(50))
    metricv3_cvss_attackComplexity = db.Column(db.String(50))
    metricv3_cvss_privilegesRequired = db.Column(db.String(50))
    metricv3_cvss_userInteraction = db.Column(db.String(50))
    metricv3_cvss_scope = db.Column(db.String(50))
    metricv3_cvss_confidentialityImpact = db.Column(db.String(50))
    metricv3_cvss_integrityImpact = db.Column(db.String(50))
    metricv3_cvss_availabilityImpact = db.Column(db.String(50))
    metricv3_cvss_baseScore = db.Column(db.Float())
    metricv3_cvss_baseSeverity = db.Column(db.String(50))

    metricv3_exploitability_score = db.Column(db.Float())
    metricv3_impact_score = db.Column(db.Float())

    Code_Execution = db.Column(db.String(10))
    Product_CPE = db.Column(db.Text())
    Platform_CPE = db.Column(db.Text())
    Network_Service = db.Column(db.String(20))
    Network_Protocol = db.Column(db.String(10))

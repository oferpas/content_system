from flask_admin.contrib.sqla import ModelView, filters
from werkzeug.exceptions import HTTPException
import flask_admin.contrib.sqla
from App.config import app, db
import hashlib
from io import BytesIO
import datetime
from flask_admin import expose
from flask import request, Response, send_file
from App.db import PurpleCVE, PurpleCPE, PurpleContentFiles
from flask_admin.model.template import EndpointLinkRowAction
from flask_admin.contrib.sqla.filters import FilterEqual, FilterLike, FilterNotLike


class EOLView(flask_admin.contrib.sqla.ModelView):
    page_size = 20
    column_list = ('name', 'cycle', 'cycleShortHand', 'lts', 'release', 'support', 'eol', 'latest', 'link')
    column_searchable_list = ['name', 'cycle', 'cycleShortHand', 'lts', 'release', 'support', 'eol', 'latest', 'link']

    def is_accessible(self):
        auth = request.authorization or request.environ.get('REMOTE_USER')  # workaround for Apache
        if not auth or (auth.username, auth.password) != app.config['ADMIN_CREDENTIALS']:
            raise HTTPException('', Response("FUCK OFF.", 401, {'WWW-Authenticate': 'Basic realm="Login Required"'}))
        return True


class ApplicationView(flask_admin.contrib.sqla.ModelView):
    page_size = 20
    column_list = ('Application_Name', 'Application_Service', 'TCP', 'UDP')
    column_searchable_list = ['Application_Name', 'Application_Service', 'TCP', 'UDP']

    def is_accessible(self):
        auth = request.authorization or request.environ.get('REMOTE_USER')  # workaround for Apache
        if not auth or (auth.username, auth.password) != app.config['ADMIN_CREDENTIALS']:
            raise HTTPException('', Response("FUCK OFF.", 401, {'WWW-Authenticate': 'Basic realm="Login Required"'}))
        return True


class KbView(flask_admin.contrib.sqla.ModelView):
    page_size = 20
    column_searchable_list = ['cve', 'description', 'ExploitStatus', 'FullProductName', 'RemediationType',
                              'RestartRequired', 'ProductId', 'Supercedence', 'UrlBinaries', 'UrlFix', 'VendorFix']
    column_list = ('cve', 'description', 'ExploitStatus', 'FullProductName', 'RemediationType', 'RestartRequired', 'ProductId', 'Supercedence', 'UrlBinaries', 'UrlFix', 'VendorFix')

    def is_accessible(self):
        auth = request.authorization or request.environ.get('REMOTE_USER')  # workaround for Apache
        if not auth or (auth.username, auth.password) != app.config['ADMIN_CREDENTIALS']:
            raise HTTPException('', Response("FUCK OFF.", 401, {'WWW-Authenticate': 'Basic realm="Login Required"'}))
        return True


class FilesView(flask_admin.contrib.sqla.ModelView):
    page_size = 20

    column_list = ('filename', 'http_status', 'file_status', 'MD5', 'date')
    column_extra_row_actions = [EndpointLinkRowAction('glyphicon glyphicon-download-alt', '.get_results')]

    @expose('/download/<id>', methods=['GET'])
    def get_results(self, id):
        file_details = db.session.query(PurpleContentFiles).filter_by(id=id).one()
        content = file_details.file_content
        buffer = BytesIO()
        buffer.write(content.encode('utf-8'))
        buffer.seek(0)

        return send_file(
            buffer,
            as_attachment=True,
            attachment_filename=file_details.filename,
            mimetype='text/plain'
        )

    def is_accessible(self):
        auth = request.authorization or request.environ.get('REMOTE_USER')  # workaround for Apache
        if not auth or (auth.username, auth.password) != app.config['ADMIN_CREDENTIALS']:
            raise HTTPException('', Response("FUCK OFF.", 401, {'WWW-Authenticate': 'Basic realm="Login Required"'}))
        return True


def get_cvss_v2_vectorString():
    cvss_v2_vectorString = PurpleCVE.query.with_entities(PurpleCVE.metricv2_cvss_vectorString.distinct().label("metricv2_cvss_vectorString")).order_by(PurpleCVE.metricv2_cvss_vectorString.asc()).all()
    return [(item.metricv2_cvss_vectorString, item.metricv2_cvss_vectorString) for item in cvss_v2_vectorString]


def get_cvss_v2_accessVector():
    cvss_v2_accessVector = PurpleCVE.query.with_entities(PurpleCVE.metricv2_cvss_accessVector.distinct().label("metricv2_cvss_accessVector")).order_by(PurpleCVE.metricv2_cvss_accessVector.asc()).all()
    return [(item.metricv2_cvss_accessVector, item.metricv2_cvss_accessVector) for item in cvss_v2_accessVector]


def get_cvss_v2_accessComplexity():
    cvss_v2_accessComplexity = PurpleCVE.query.with_entities(PurpleCVE.metricv2_cvss_accessComplexity.distinct().label("metricv2_cvss_accessComplexity")).order_by(PurpleCVE.metricv2_cvss_accessComplexity.asc()).all()
    return [(item.metricv2_cvss_accessComplexity, item.metricv2_cvss_accessComplexity) for item in cvss_v2_accessComplexity]


def get_cvss_v2_authentication():
    cvss_v2_authentication = PurpleCVE.query.with_entities(PurpleCVE.metricv2_cvss_authentication.distinct().label("metricv2_cvss_authentication")).order_by(PurpleCVE.metricv2_cvss_authentication.asc()).all()
    return [(item.metricv2_cvss_authentication, item.metricv2_cvss_authentication) for item in cvss_v2_authentication]


def get_cvss_v2_confidentialityImpact():
    cvss_v2_confidentialityImpact = PurpleCVE.query.with_entities(PurpleCVE.metricv2_cvss_confidentialityImpact.distinct().label("metricv2_cvss_confidentialityImpact")).order_by(PurpleCVE.metricv2_cvss_confidentialityImpact.asc()).all()
    return [(item.metricv2_cvss_confidentialityImpact, item.metricv2_cvss_confidentialityImpact) for item in cvss_v2_confidentialityImpact]


def get_cvss_v2_integrityImpact():
    cvss_v2_integrityImpact = PurpleCVE.query.with_entities(PurpleCVE.metricv2_cvss_integrityImpact.distinct().label("metricv2_cvss_integrityImpact")).order_by(PurpleCVE.metricv2_cvss_integrityImpact.asc()).all()
    return [(item.metricv2_cvss_integrityImpact, item.metricv2_cvss_integrityImpact) for item in cvss_v2_integrityImpact]


def get_cvss_v2_availabilityImpact():
    cvss_v2_availabilityImpact = PurpleCVE.query.with_entities(PurpleCVE.metricv2_cvss_availabilityImpact.distinct().label("metricv2_cvss_availabilityImpact")).order_by(PurpleCVE.metricv2_cvss_availabilityImpact.asc()).all()
    return [(item.metricv2_cvss_availabilityImpact, item.metricv2_cvss_availabilityImpact) for item in cvss_v2_availabilityImpact]


def get_cvss_v2_baseScore():
    cvss_v2_baseScore = PurpleCVE.query.with_entities(PurpleCVE.metricv2_cvss_baseScore.distinct().label("metricv2_cvss_baseScore")).order_by(PurpleCVE.metricv2_cvss_baseScore.asc()).all()
    return [(item.metricv2_cvss_baseScore, item.metricv2_cvss_baseScore) for item in cvss_v2_baseScore]


def get_metric_v2_severity():
    metricv2_severity = PurpleCVE.query.with_entities(PurpleCVE.metricv2_severity.distinct().label("metricv2_severity")).order_by(PurpleCVE.metricv2_severity.asc()).all()
    return [(item.metricv2_severity, item.metricv2_severity) for item in metricv2_severity]


def get_metricv2_exploitability_score():
    cvss_v2_exploitability_score = PurpleCVE.query.with_entities(PurpleCVE.metricv2_exploitability_score.distinct().label("metricv2_exploitability_score")).order_by(PurpleCVE.metricv2_exploitability_score.asc()).all()
    return [(item.metricv2_exploitability_score, item.metricv2_exploitability_score) for item in cvss_v2_exploitability_score]


def get_metricv2_impact_score():
    cvss_v2_impact_score = PurpleCVE.query.with_entities(PurpleCVE.metricv2_impact_score.distinct().label("metricv2_impact_score")).order_by(PurpleCVE.metricv2_impact_score.asc()).all()
    return [(item.metricv2_impact_score, item.metricv2_impact_score) for item in cvss_v2_impact_score]


def get_metricv2_acInsufInfo():
    cvss_v2_acInsufInfo = PurpleCVE.query.with_entities(PurpleCVE.metricv2_acInsufInfo.distinct().label("metricv2_acInsufInfo")).order_by(PurpleCVE.metricv2_acInsufInfo.asc()).all()
    return [(item.metricv2_acInsufInfo, item.metricv2_acInsufInfo) for item in cvss_v2_acInsufInfo]


def get_metricv2_obtainAllPrivilege():
    cvss_v2_obtainAllPrivilege = PurpleCVE.query.with_entities(PurpleCVE.metricv2_obtainAllPrivilege.distinct().label("metricv2_obtainAllPrivilege")).order_by(PurpleCVE.metricv2_obtainAllPrivilege.asc()).all()
    return [(item.metricv2_obtainAllPrivilege, item.metricv2_obtainAllPrivilege) for item in cvss_v2_obtainAllPrivilege]


def get_metricv2_obtainUserPrivilege():
    cvss_v2_obtainUserPrivilege = PurpleCVE.query.with_entities(PurpleCVE.metricv2_obtainUserPrivilege.distinct().label("metricv2_obtainUserPrivilege")).order_by(PurpleCVE.metricv2_obtainUserPrivilege.asc()).all()
    return [(item.metricv2_obtainUserPrivilege, item.metricv2_obtainUserPrivilege) for item in cvss_v2_obtainUserPrivilege]


def get_metricv2_obtainOtherPrivilege():
    cvss_v2_obtainOtherPrivilege = PurpleCVE.query.with_entities(PurpleCVE.metricv2_obtainOtherPrivilege.distinct().label("metricv2_obtainOtherPrivilege")).order_by(PurpleCVE.metricv2_obtainOtherPrivilege.asc()).all()
    return [(item.metricv2_obtainOtherPrivilege, item.metricv2_obtainOtherPrivilege) for item in cvss_v2_obtainOtherPrivilege]


def get_metricv2_userInteractionRequired():
    cvss_v2_userInteractionRequired = PurpleCVE.query.with_entities(PurpleCVE.metricv2_userInteractionRequired.distinct().label("metricv2_userInteractionRequired")).order_by(PurpleCVE.metricv2_userInteractionRequired.asc()).all()
    return [(item.metricv2_userInteractionRequired, item.metricv2_userInteractionRequired) for item in cvss_v2_userInteractionRequired]


def get_cvss_metricv3_vectorString():
    cvss_v3_vectorString = PurpleCVE.query.with_entities(PurpleCVE.metricv3_cvss_vectorString.distinct().label("metricv3_cvss_vectorString")).order_by(PurpleCVE.metricv3_cvss_vectorString.asc()).all()
    return [(item.metricv3_cvss_vectorString, item.metricv3_cvss_vectorString) for item in cvss_v3_vectorString]


def get_cvss_metricv3_attackVector():
    cvss_v3_vectorString = PurpleCVE.query.with_entities(PurpleCVE.metricv3_cvss_attackVector.distinct().label("metricv3_cvss_attackVector")).order_by(PurpleCVE.metricv3_cvss_attackVector.asc()).all()
    return [(item.metricv3_cvss_attackVector, item.metricv3_cvss_attackVector) for item in cvss_v3_vectorString]


def get_cvss_metricv3_attackComplexity():
    cvss_v3_attackComplexity = PurpleCVE.query.with_entities(PurpleCVE.metricv3_cvss_attackComplexity.distinct().label("metricv3_cvss_attackComplexity")).order_by(PurpleCVE.metricv3_cvss_attackComplexity.asc()).all()
    return [(item.metricv3_cvss_attackComplexity, item.metricv3_cvss_attackComplexity) for item in cvss_v3_attackComplexity]


def get_cvss_metricv3_privilegesRequired():
    cvss_v3_privilegesRequired = PurpleCVE.query.with_entities(PurpleCVE.metricv3_cvss_privilegesRequired.distinct().label("metricv3_cvss_privilegesRequired")).order_by(PurpleCVE.metricv3_cvss_privilegesRequired.asc()).all()
    return [(item.metricv3_cvss_privilegesRequired, item.metricv3_cvss_privilegesRequired) for item in cvss_v3_privilegesRequired]


def get_cvss_metricv3_userInteraction():
    cvss_v3_userInteraction = PurpleCVE.query.with_entities(PurpleCVE.metricv3_cvss_userInteraction.distinct().label("metricv3_cvss_userInteraction")).order_by(PurpleCVE.metricv3_cvss_userInteraction.asc()).all()
    return [(item.metricv3_cvss_userInteraction, item.metricv3_cvss_userInteraction) for item in cvss_v3_userInteraction]


def get_cvss_metricv3_scope():
    cvss_v3_scope = PurpleCVE.query.with_entities(PurpleCVE.metricv3_cvss_scope.distinct().label("metricv3_cvss_scope")).order_by(PurpleCVE.metricv3_cvss_scope.asc()).all()
    return [(item.metricv3_cvss_scope, item.metricv3_cvss_scope) for item in cvss_v3_scope]


def get_cvss_metricv3_confidentialityImpact():
    cvss_v3_confidentialityImpact = PurpleCVE.query.with_entities(PurpleCVE.metricv3_cvss_confidentialityImpact.distinct().label("metricv3_cvss_confidentialityImpact")).order_by(PurpleCVE.metricv3_cvss_confidentialityImpact.asc()).all()
    return [(item.metricv3_cvss_confidentialityImpact, item.metricv3_cvss_confidentialityImpact) for item in cvss_v3_confidentialityImpact]


def get_cvss_metricv3_integrityImpact():
    cvss_v3_integrityImpact = PurpleCVE.query.with_entities(PurpleCVE.metricv3_cvss_integrityImpact.distinct().label("metricv3_cvss_integrityImpact")).order_by(PurpleCVE.metricv3_cvss_integrityImpact.asc()).all()
    return [(item.metricv3_cvss_integrityImpact, item.metricv3_cvss_integrityImpact) for item in cvss_v3_integrityImpact]


def get_cvss_metricv3_availabilityImpact():
    cvss_v3_availabilityImpact = PurpleCVE.query.with_entities(PurpleCVE.metricv3_cvss_availabilityImpact.distinct().label("metricv3_cvss_availabilityImpact")).order_by(PurpleCVE.metricv3_cvss_availabilityImpact.asc()).all()
    return [(item.metricv3_cvss_availabilityImpact, item.metricv3_cvss_availabilityImpact) for item in cvss_v3_availabilityImpact]


def get_cvss_metricv3_baseScore():
    cvss_v3_baseScore = PurpleCVE.query.with_entities(PurpleCVE.metricv3_cvss_baseScore.distinct().label("metricv3_cvss_baseScore")).order_by(PurpleCVE.metricv3_cvss_baseScore.asc()).all()
    return [(item.metricv3_cvss_baseScore, item.metricv3_cvss_baseScore) for item in cvss_v3_baseScore]


def get_cvss_metricv3_baseSeverity():
    cvss_v3_baseSeverity = PurpleCVE.query.with_entities(PurpleCVE.metricv3_cvss_baseSeverity.distinct().label("metricv3_cvss_baseSeverity")).order_by(PurpleCVE.metricv3_cvss_baseSeverity.asc()).all()
    return [(item.metricv3_cvss_baseSeverity, item.metricv3_cvss_baseSeverity) for item in cvss_v3_baseSeverity]


def get_cvss_metricv3_exploitability_score():
    cvss_v3_exploitability_score = PurpleCVE.query.with_entities(PurpleCVE.metricv3_exploitability_score.distinct().label("metricv3_exploitability_score")).order_by(PurpleCVE.metricv3_exploitability_score.asc()).all()
    return [(item.metricv3_exploitability_score, item.metricv3_exploitability_score) for item in cvss_v3_exploitability_score]


def get_cvss_metricv3_impact_score():
    cvss_v3_impact_score = PurpleCVE.query.with_entities(PurpleCVE.metricv3_impact_score.distinct().label("metricv3_impact_score")).order_by(PurpleCVE.metricv3_impact_score.asc()).all()
    return [(item.metricv3_impact_score, item.metricv3_impact_score) for item in cvss_v3_impact_score]


def get_cpe_product():
    cpe_name = PurpleCPE.query.with_entities(PurpleCPE.Product_CPE.distinct().label("Product_CPE")).order_by(PurpleCPE.Product_CPE.asc()).all()
    return [(item.Product_CPE, item.Product_CPE) for item in cpe_name]


def get_cpe_platform():
    cpe_name = PurpleCPE.query.with_entities(PurpleCPE.Platform_CPE.distinct().label("Platform_CPE")).order_by(PurpleCPE.Platform_CPE.asc()).all()
    return [(item.Platform_CPE, item.Platform_CPE) for item in cpe_name]


class AdminView(flask_admin.contrib.sqla.ModelView):

    can_delete = False
    page_size = 30
    column_list = ['cve', 'cwe', 'description', ]
    column_searchable_list = ['cve', 'cwe', 'references', 'description', 'metricv2_cvss_vectorString', 'metricv3_cvss_vectorString', 'Platform_CPE', 'Product_CPE']
    form_choices = {'Code_Execution': [('RCE', 'RCE'), ('LCE', 'LCE'), ('PRIVESC', 'PRIVESC'), ('DOS', 'DOS'), ('SCE', 'SCE')],
                    'Network_Protocol': [('TCP', 'TCP'), ('UDP', 'UDP')]}

    column_filters = [
        'description',
        'Code_Execution',
        FilterEqual(column=PurpleCVE.metricv2_cvss_vectorString, name='CVSSv2 Vector_String', options=get_cvss_v2_vectorString),
        FilterEqual(column=PurpleCVE.metricv2_cvss_accessVector, name='CVSSv2 Access_Vector', options=get_cvss_v2_accessVector),
        FilterEqual(column=PurpleCVE.metricv2_cvss_accessComplexity, name='CVSSv2 Access_Complexity', options=get_cvss_v2_accessComplexity),
        FilterEqual(column=PurpleCVE.metricv2_cvss_authentication, name='CVSSv2 Access_Complexity', options=get_cvss_v2_authentication),
        FilterEqual(column=PurpleCVE.metricv2_cvss_confidentialityImpact, name='CVSSv2 Confidentiality_Impact', options=get_cvss_v2_confidentialityImpact),
        FilterEqual(column=PurpleCVE.metricv2_cvss_integrityImpact, name='CVSSv2 Integrity_Impact', options=get_cvss_v2_integrityImpact),
        FilterEqual(column=PurpleCVE.metricv2_cvss_availabilityImpact, name='CVSSv2 Availability_Impact', options=get_cvss_v2_availabilityImpact),
        FilterEqual(column=PurpleCVE.metricv2_cvss_baseScore, name='CVSSv2 Base_Score', options=get_cvss_v2_baseScore),
        FilterEqual(column=PurpleCVE.metricv2_severity, name='CVSSv2 severity', options=get_metric_v2_severity),
        FilterEqual(column=PurpleCVE.metricv2_exploitability_score, name='CVSSv2 Exploitability_Score', options=get_metricv2_exploitability_score),
        FilterEqual(column=PurpleCVE.metricv2_impact_score, name='CVSSv2 Impact_Score', options=get_metricv2_impact_score),
        FilterEqual(column=PurpleCVE.metricv2_acInsufInfo, name='CVSSv2 acInsufInfo', options=get_metricv2_acInsufInfo),
        FilterEqual(column=PurpleCVE.metricv2_obtainAllPrivilege, name='CVSSv2 obtainAllPrivilege', options=get_metricv2_obtainAllPrivilege),
        FilterEqual(column=PurpleCVE.metricv2_obtainUserPrivilege, name='CVSSv2 obtainUserPrivilege', options=get_metricv2_obtainUserPrivilege),
        FilterEqual(column=PurpleCVE.metricv2_obtainOtherPrivilege, name='CVSSv2 obtainOtherPrivilege', options=get_metricv2_obtainOtherPrivilege),
        FilterEqual(column=PurpleCVE.metricv2_userInteractionRequired, name='CVSSv2 userInteractionRequired', options=get_metricv2_userInteractionRequired),
        FilterEqual(column=PurpleCVE.metricv3_cvss_vectorString, name='CVSSv3 Vector_String', options=get_cvss_metricv3_vectorString),
        FilterEqual(column=PurpleCVE.metricv3_cvss_attackVector, name='CVSSv3 Attack_Vector', options=get_cvss_metricv3_attackVector),
        FilterEqual(column=PurpleCVE.metricv3_cvss_privilegesRequired, name='CVSSv3 PrivilegesRequired', options=get_cvss_metricv3_privilegesRequired),
        FilterEqual(column=PurpleCVE.metricv3_cvss_userInteraction, name='CVSSv3 UserInteraction', options=get_cvss_metricv3_userInteraction),
        FilterEqual(column=PurpleCVE.metricv3_cvss_scope, name='CVSSv3 Scope', options=get_cvss_metricv3_scope),
        FilterEqual(column=PurpleCVE.metricv3_cvss_confidentialityImpact, name='CVSSv3 ConfidentialityImpact', options=get_cvss_metricv3_confidentialityImpact),
        FilterEqual(column=PurpleCVE.metricv3_cvss_integrityImpact, name='CVSSv3 IntegrityImpact', options=get_cvss_metricv3_integrityImpact),
        FilterEqual(column=PurpleCVE.metricv3_cvss_baseScore, name='CVSSv3 BaseScore', options=get_cvss_metricv3_baseScore),
        FilterEqual(column=PurpleCVE.metricv3_cvss_baseSeverity, name='CVSSv3 BaseSeverity', options=get_cvss_metricv3_baseSeverity),
        FilterEqual(column=PurpleCVE.metricv3_exploitability_score, name='CVSSv3 Exploitability_Score', options=get_cvss_metricv3_exploitability_score),
        FilterEqual(column=PurpleCVE.metricv3_impact_score, name='CVSSv3 Impact_Score', options=get_cvss_metricv3_impact_score),
        FilterLike(column=PurpleCVE.Product_CPE, name='CPE Product', options=get_cpe_product),
        FilterLike(column=PurpleCVE.Platform_CPE, name='CPE Platform', options=get_cpe_platform),
    ]

    def is_accessible(self):
        auth = request.authorization or request.environ.get('REMOTE_USER')  # workaround for Apache
        if not auth or (auth.username, auth.password) != app.config['ADMIN_CREDENTIALS']:
            raise HTTPException('', Response("FUCK OFF.", 401, {'WWW-Authenticate': 'Basic realm="Login Required"'}))
        return True
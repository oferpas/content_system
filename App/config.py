from flask import Flask
from flask_admin import Admin
from flask_sqlalchemy import SQLAlchemy

# CONFIG #

SSHConfig = {
    'server': '192.168.0.15',
    'username': 'manager',
    'password': 'f8712462',

    'json_filename': 'cve_to_product.json',
    'get_latest_directory': "stat -c '%y - %n' /home/manager/collector/collector/data/* | sort -r -t'-' -k1,1 | head -n 1 | cut -d \" \" -f 5"
}

app = Flask(__name__)
# app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:f8712462@localhost/cve'
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://dbuser:f8712462@192.168.109.75/cve'
app.config['SECRET_KEY'] = 'SecretKey'
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True
app.config['ADMIN_CREDENTIALS'] = ('admin', 'pa$$word')
app.config['JSONIFY_PRETTYPRINT_REGULAR'] = True

app.config['EXECUTOR_TYPE'] = 'thread'
app.config['EXECUTOR_MAX_WORKERS'] = 10


# set optional bootswatch theme
# see http://bootswatch.com/3/ for available swatches
app.config['FLASK_ADMIN_SWATCH'] = 'cosmo'

db = SQLAlchemy(app)
admin = Admin(app, name='CVE Based Content System', template_mode='bootstrap3')
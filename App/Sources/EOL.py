import json
import requests
from bs4 import BeautifulSoup


parsed = {}
base_url = 'https://endoflife.date/'
json_base_url = 'https://endoflife.date/api/{0}.json'
keys = ['cycle', 'cycleShortHand', 'lts', 'release', 'support', 'eol', 'latest', 'link']


def get_products():
    products = {}
    page = requests.get(base_url)

    soup = BeautifulSoup(page.content, "html.parser")
    results = soup.find("ul", class_="nav-list")
    for result in results:
        products[result.text] = result.find(href=True)['href'].lstrip('/')

    return products

def get_json(url):
    return json.loads(requests.get(url).text)


def parse_json(raw):
    data = []
    for item in raw:
        tmp = {}
        for key in keys:
            if key not in item.keys():
                continue
            tmp[key] = item.get(key)
        data.append(tmp)
    return data

products = get_products()
for product in products:
    raw = get_json(json_base_url.format(products.get(product)))
    parsed[product] = parse_json(raw)

with open('EOL_json', "w") as f:
    json.dump(parsed, f, indent=4)
print(parsed)
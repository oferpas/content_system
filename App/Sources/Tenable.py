import re
import json
import time
import requests

import queue
from fake_useragent import UserAgent
from tqdm import tqdm
from tqdm.contrib.concurrent import thread_map


# Variables
jobs_queue = queue.Queue()
classification = {}


MinID = 10001 #10001
MaxID = 152737
RequestCounter = 0

BaseURL = 'https://www.tenable.com/plugins/nessus/{0}'
# DataRegex = 'type="application\/json">(.*)<\/script><script nomodule='
DataRegex = 'type="application\/json" nonce="nonce-\w{48}">(.*)<\/script><script'
headers = {
        'User-Agent':  UserAgent().random,
}


def get_all_plugins():
    plugin_data = []

    counter = 0
    # tqdm(cybot_cve_list.get('CVES'), desc='Commited CVES:'):
    for plugin_id in tqdm(range(MinID, MaxID), desc='Parsed Plugins'):
    # for plugin_id in range(MinID, MaxID):
        if counter == 400:
            time.sleep(60)
        pluginURL = BaseURL.format(plugin_id)
        response = requests.get(pluginURL, headers=headers)
        counter += 1
        # print('Plugin ID: {0}, Response Code: {1}, Response Length: {2}'.format(plugin_id, response.status_code, len(response.text)))
        plugin_data.append(parse_plugin(plugin_id, response.text)) if response.status_code == 200 else None
        time.sleep(0.1)

    return plugin_data


def get_html(url):
    response = requests.get(url, headers=headers)
    return response.text if response.status_code == 200 else None


def parse_plugin(plugin_id):

    data = get_html(BaseURL.format(plugin_id))
    if data is None:
        return
    regex = re.search(DataRegex, data).group(1)
    if not regex:
        return

    plugin_json = json.loads(regex)['props']['pageProps']['plugin']
    time.sleep(0.5)
    return {
        'plugin_id': str(plugin_id),
        'cvss_base_score': str(plugin_json.get('cvss_base_score')),
        'exploitability_ease': str(plugin_json.get('exploitability_ease')),
        'cpe': plugin_json.get('cpe'),
        'cvss_temporal_score': str(plugin_json.get('cvss_temporal_score')),
        'potential_vulnerability': str(plugin_json.get('potential_vulnerability')),
        'cwe': str(plugin_json.get('cwe')),
        'msft': str(plugin_json.get('msft')),
        'cves': plugin_json.get('cves'),
        'patch_publication_date': str(plugin_json.get('patch_publication_date')),
        'risk_factor': str(plugin_json.get('risk_factor')),
        'filename': str(plugin_json.get('filename')),
        'vuln_publication_date': str(plugin_json.get('vuln_publication_date')),
        'description': plugin_json.get('description'),
        'risk_factor_score': str(plugin_json.get('risk_factor_score')),
        'cvss_vector': str(plugin_json.get('cvss_vector')),
        'script_required_ports': str(plugin_json.get('script_required_ports')),
        'script_family': str(plugin_json.get('script_family')),
        'solution': str(plugin_json.get('solution')),
        'synopsis': str(plugin_json.get('synopsis')),
        'exploit_available': str(plugin_json.get('exploit_available')),
        'plugin_type': str(plugin_json.get('plugin_type')),
        'exploit_framework_metasploit': str(plugin_json.get('exploit_framework_metasploit')),
        'metasploit_name': str(plugin_json.get('metasploit_name')),
    }



def scrape():
    plugins = []
    [plugins.append(plugin) for plugin in range(MinID, MaxID)]
    results = thread_map(parse_plugin, plugins, max_workers=2)
    print(results)


# get_plugin('34743')
#get_all_plugins()
scrape()
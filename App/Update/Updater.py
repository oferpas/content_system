import os
import csv
import json
import gzip
import hashlib
import paramiko
import requests
from tqdm import tqdm
from App.config import db
from bs4 import BeautifulSoup
from App.config import SSHConfig
from datetime import date, datetime
from App.Update.NvdLoader import NVDLoad
from multiprocessing.pool import ThreadPool
from App.Update.UpdateCVEDetails import CVEDetails
from App.db import PurpleCVE, PurpleContentFiles, PurpleEOL, PurpleCveToKb, PurpleApplication
# from App.Classifier.VulnClassifier import AutomatedVulnerabilityClassifier
from tqdm.contrib.concurrent import thread_map


class Updater:

    def __init__(self):
        db.create_all()
        self.cpe_data = None
        self.nvd_vulnerabilities = {}
        self.nvd_files_names = [2002, date.today().year, 'recent']
        self.eol_url = 'https://endoflife.date/'
        self.eol_url_json = 'https://endoflife.date/api/{0}.json'
        self.nvd_json_url = 'https://nvd.nist.gov/feeds/json/cve/1.1/nvdcve-1.1-{0}.json.gz'
        self.cve_to_kb_url = 'http://192.168.109.28:8080/api/cve/CveAllProducts?vendor=Microsoft&cve={0}'
        self.application_document = 'Applications_to_service.csv'
        self.nvd_files = [item.filename for item in PurpleContentFiles.query.all() if 'nvdcve' in item.filename]
        [self.nvd_vulnerabilities.update(NVDLoad('{0}'.format(filename)).cves) for filename in self.nvd_files]

    def get_cpe_data(self):
        with open(SSHConfig.get('json_filename')) as f:
            self.cpe_data = json.loads(f)

    def filename_downloader(self, url):
        request = requests.get(url)
        return request if request is not None and request.status_code == 200 else None

    def get_kb_for_cve(self, cve):

        items = []
        request = self.filename_downloader(self.cve_to_kb_url.format(cve))
        if request is None:
            return

        data = json.loads(request.text())
        if not data:
            return

        cve = data[0].get('Cve')
        description = data[0].get('Notes').get('Description')

        if PurpleCveToKb.query.filter_by(cve=cve).count() <= 0:
            for product in data[0].get('Products'):
                item = {}
                item['cve'] = cve
                item['description'] = description
                item['ExploitStatus'] = product.get('ExploitStatus')
                item['FullProductName'] = product.get('FullProductName')
                item['RemediationType'] = product.get('RemediationType')
                item['RestartRequired'] = product.get('RestartRequired')
                item['ProductId'] = product.get('ProductId')
                item['Supercedence'] = product.get('Supercedence')
                item['UrlBinaries'] = product.get('UrlBinaries')
                item['UrlFix'] = product.get('UrlFix')
                item['VendorFix'] = product.get('VendorFix')
                items.append(item)

            for entry in items:
                db.session.add(PurpleCveToKb(**entry))
            db.session.commit()
        else:
            return

    def get_eol_data(self):

        def parse_product_json(json_data):
            data = []
            for item in json_data:
                tmp = {}
                for key in keys:
                    if key not in item.keys():
                        continue
                    tmp[key] = item.get(key)
                data.append(tmp)
            return data

        products = {}
        keys = ['cycle', 'cycleShortHand', 'lts', 'release', 'support', 'eol', 'latest', 'link']
        page = requests.get(self.eol_url)

        soup = BeautifulSoup(page.content, "html.parser")
        results = soup.find("ul", class_="nav-list")
        for result in results:
            products[result.text] = result.find(href=True)['href'].lstrip('/')

        for product in products:
            json_data = json.loads(requests.get(self.eol_url_json.format(products.get(product))).text)
            items_list = parse_product_json(json_data)

            for item in items_list:

                # name = item.get('cycle') if product in item.get('cycle') else product
                name = item.get('cycle') if product in item.get('cycle') else "{} {}".format(product, item.get('cycle'))
                md5_signature = {'name': name,
                                 'release': item.get('release'),
                                 'support': item.get('support'),
                                 'eol': item.get('eol'),
                                 }

                if PurpleEOL.query.filter_by(name=name).count() <= 0:
                    entry = {
                        'name': name,
                        'cycle': item.get('cycle'),
                        'cycleShortHand': item.get('cycleShortHand'),
                        'lts': item.get('lts'),
                        'release': item.get('release'),
                        'support': item.get('support'),
                        'eol': item.get('eol'),
                        'latest': item.get('latest'),
                        'link': item.get('link'),
                        'MD5': hashlib.md5(json.dumps(md5_signature).encode("utf-8")).hexdigest()
                    }
                    db.session.add(PurpleEOL(**entry))
                    db.session.commit()

                else:
                    record = db.session.query(PurpleEOL).filter_by(name=name).one()
                    if record.MD5 != hashlib.md5(json.dumps(md5_signature).encode("utf-8")).hexdigest():
                        record.name = name
                        record.cycle = item.get('cycle')
                        record.cycleShortHand = item.get('cycleShortHand'),
                        record.lts = item.get('lts')
                        record.release = item.get('release')
                        record.support = item.get('support')
                        record.eol = item.get('eol')
                        record.latest = item.get('latest')
                        record.link = item.get('link')
                        record.MD5 = hashlib.md5(json.dumps(md5_signature).encode("utf-8")).hexdigest()
                        db.session.commit()

    def get_cve_details_files(self):

        data = {}
        pool = ThreadPool(20)
        cve_list = [i.cve for i in PurpleCVE.query.all()]
        results = pool.map(CVEDetails, cve_list)
        pool.close()
        pool.join()
        for result in results:
            if result:
                print(result)


        # for cve in cve_list:
        #
        #     results = CVEDetails(cve=cve).results
        #     if results:
        #         data[cve] = results

        entry = {
            'filename': 'cve-details',
            'file_status': 'Updated',
            'http_status': 200,
            'file_content': json.dumps(data),
            'MD5': hashlib.md5(json.dumps(data).encode('utf-8')).hexdigest(),
            'date': datetime.now().strftime("%d/%m/%Y %H:%M:%S")
        }

        db.session.add(PurpleContentFiles(**entry))
        db.session.commit()

    def update_nvd_files(self):
        item_list = [self.nvd_files_names[2]]
        item_list.extend(list(map(str, range(self.nvd_files_names[0], self.nvd_files_names[1]))))
        item_list.sort()
        for item in item_list:
            filename = self.nvd_json_url.format(item).rsplit('/')[-1].strip('.gz')
            file_request = self.filename_downloader(self.nvd_json_url.format(item))
            if file_request is None:
                continue

            if PurpleContentFiles.query.filter_by(filename=filename).count() <= 0:
                if file_request is None:
                    entry = {
                        'filename': filename,
                        'file_status': 'Failed',
                        'http_status': file_request.status_code,
                        'date': datetime.now().strftime("%d/%m/%Y %H:%M:%S")
                    }
                else:
                    entry = {
                        'filename': filename,
                        'file_status': 'Updated',
                        'http_status': file_request.status_code,
                        'file_content': json.dumps(json.loads(gzip.decompress(file_request.content))),
                        'MD5': hashlib.md5(json.dumps(json.loads(gzip.decompress(file_request.content))).encode('utf-8')).hexdigest(),
                        'date': datetime.now().strftime("%d/%m/%Y %H:%M:%S")

                    }
                db.session.add(PurpleContentFiles(**entry))
                db.session.commit()

            else:
                file_details = db.session.query(PurpleContentFiles).filter_by(filename=filename).one()

                if file_request is None:
                    file_details.file_status = 'Failed'
                    file_details.date = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
                else:
                    downloaded_md5 = hashlib.md5(json.dumps(json.loads(gzip.decompress(file_request.content))).encode('utf-8')).hexdigest()
                    if file_details.MD5 != downloaded_md5:
                        file_details.file_status = 'Updated'
                        file_details.http_status = file_request.status_code
                        file_details.file_content = json.dumps(json.loads(gzip.decompress(file_request.content)))
                        file_details.MD5 = hashlib.md5(json.dumps(json.loads(gzip.decompress(file_request.content))).encode('utf-8')).hexdigest()
                        file_details.date = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
                    else:
                        file_details.date = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
                    db.session.commit()

        self.nvd_files = [item.filename for item in PurpleContentFiles.query.all() if 'nvdcve' in item.filename]
        [self.nvd_vulnerabilities.update(NVDLoad('{0}'.format(filename)).cves) for filename in self.nvd_files]

    def get_ssh_file(self):
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(SSHConfig.get('server'), username=SSHConfig.get('username'), password=SSHConfig.get('password'))

        _, ssh_stdout, _ = ssh.exec_command(SSHConfig.get('get_latest_directory'))
        full_path = '{0}/{1}'.format(ssh_stdout.read().strip('\n'), SSHConfig.get('json_filename'))

        sftp = ssh.open_sftp()
        sftp.get(full_path, SSHConfig.get('json_filename'))

    def load_application_port(self):

        data = []
        location = '{0}/App/Data/{1}'.format(os.getcwd(), self.application_document)
        with open(location, mode='r') as csv_file:
            csv_reader = csv.DictReader(csv_file)
            for row in csv_reader:
                print('Adding Application {0} To DB'.format(row.get('Application')))
                Application_Name = row.get('Application')
                Application_Service = row.get('Service') if '-' not in row.get('Service') else ''
                TCP = row.get('TCP Default ports')
                UDP = row.get('UDP Default ports')

                item = {
                    'Application_Name': Application_Name,
                    'Application_Service': Application_Service,
                    'TCP': TCP,
                    'UDP': UDP
                }
                data.append(item)
                db.session.add(PurpleApplication(**item))
                db.session.commit()

    def build_cve(self, cve_item):

        item = {
            'cve': cve_item.get('cve', ''),
            'cwe': cve_item.get('cwe', ''),
            'references': cve_item.get('references', ''),
            'description': cve_item.get('description', ''),
            'published_date': cve_item.get('published_date', ''),
            'last_modified_date': cve_item.get('last_modified_date', ''),
        }

        cpe_item = self.cpe_data.get(item.get('cve'))
        products = cpe_item.get('cpe').get('products')
        platforms = cpe_item.get('cpe').get('platforms')

        products_cpe = set([])
        for product in products:
            for product_cpe in products.get(product):
                products_cpe.add(product_cpe)

        platforms_cpe = set([])
        for platform in platforms:
            for platform_cpe in platforms.get(platform):
                platforms_cpe.add(platform_cpe)

        metricV2, metricV3 = {}, {}
        item['title'] = cpe_item.get('title')
        item['Product_CPE'] = ','.join(products_cpe)
        item['Platform_CPE'] = ','.join(platforms_cpe)
        item['From_Patch'] = cpe_item.get('from_patch')

        if cve_item.get('metricV2'):
            metricV2 = {
                'metricv2_cvss_version': cve_item.get('metricV2', '').get('cvssV2', '').get('version', ''),
                'metricv2_cvss_vectorString': cve_item.get('metricV2', '').get('cvssV2', '').get('vectorString', ''),
                'metricv2_cvss_accessVector': cve_item.get('metricV2', '').get('cvssV2', '').get('accessVector', ''),
                'metricv2_cvss_accessComplexity': cve_item.get('metricV2', '').get('cvssV2', '').get('accessComplexity',
                                                                                                     ''),
                'metricv2_cvss_authentication': cve_item.get('metricV2', '').get('cvssV2', '').get('authentication',
                                                                                                   ''),
                'metricv2_cvss_confidentialityImpact': cve_item.get('metricV2', '').get('cvssV2', '').get(
                    'confidentialityImpact', ''),
                'metricv2_cvss_integrityImpact': cve_item.get('metricV2', '').get('cvssV2', '').get('integrityImpact',
                                                                                                    ''),
                'metricv2_cvss_availabilityImpact': cve_item.get('metricV2', '').get('cvssV2', '').get(
                    'availabilityImpact', ''),
                'metricv2_cvss_baseScore': cve_item.get('metricV2', '').get('cvssV2', '').get('baseScore', 0.0),

                'metricv2_severity': cve_item.get('metricV2', '').get('severity', ''),
                'metricv2_exploitability_score': cve_item.get('metricV2', '').get('exploitability_score', 0.0),
                'metricv2_impact_score': cve_item.get('metricV2', '').get('impact_score', 0.0),
                'metricv2_acInsufInfo': cve_item.get('metricV2', '').get('acInsufInfo', False),
                'metricv2_obtainAllPrivilege': cve_item.get('metricV2', '').get('obtainAllPrivilege', False),
                'metricv2_obtainUserPrivilege': cve_item.get('metricV2', '').get('obtainUserPrivilege', False),
                'metricv2_obtainOtherPrivilege': cve_item.get('metricV2', '').get('obtainOtherPrivilege', False),
                'metricv2_userInteractionRequired': cve_item.get('metricV2', '').get('userInteractionRequired', False),
            }
            item.update(metricV2)

        if cve_item.get('metricV3'):
            metricV3 = {
                'metricv3_cvss_version': cve_item.get('metricV3', '').get('cvssV3', '').get('version', ''),
                'metricv3_cvss_vectorString': cve_item.get('metricV3', '').get('cvssV3', '').get('vectorString', ''),
                'metricv3_cvss_attackVector': cve_item.get('metricV3', '').get('cvssV3', '').get('attackVector', ''),
                'metricv3_cvss_attackComplexity': cve_item.get('metricV3', '').get('cvssV3', '').get('attackComplexity',
                                                                                                     ''),
                'metricv3_cvss_privilegesRequired': cve_item.get('metricV3', '').get('cvssV3', '').get(
                    'privilegesRequired', ''),
                'metricv3_cvss_userInteraction': cve_item.get('metricV3', '').get('cvssV3', '').get('userInteraction',
                                                                                                    ''),
                'metricv3_cvss_scope': cve_item.get('metricV3', '').get('cvssV3', '').get('scope', ''),
                'metricv3_cvss_confidentialityImpact': cve_item.get('metricV3', '').get('cvssV3', '').get(
                    'confidentialityImpact', ''),
                'metricv3_cvss_integrityImpact': cve_item.get('metricV3', '').get('cvssV3', '').get('integrityImpact',
                                                                                                    ''),
                'metricv3_cvss_availabilityImpact': cve_item.get('metricV3', '').get('availabilityImpact', ''),
                'metricv3_cvss_baseScore': cve_item.get('metricV3', '').get('baseScore', 0.0),
                'metricv3_cvss_baseSeverity': cve_item.get('metricV3', '').get('baseSeverity', ''),
                'metricv3_exploitability_score': cve_item.get('metricV3', '').get('exploitability_score', 0.0),
                'metricv3_impact_score': cve_item.get('metricV3', '').get('impact_score', 0.0),

            }
            item.update(metricV3)

            md5_signature = {'description': cve_item.get('description', '')}
            if metricV2:
                md5_signature.update(metricV2)
            if metricV3:
                md5_signature.update(metricV3)
            item['MD5'] = hashlib.md5(json.dumps(md5_signature).encode("utf-8")).hexdigest()

        if PurpleCVE.query.filter_by(cve=cve_item.get('cve', '')).count() > 0:
            cve_object = PurpleCVE.query.filter_by(cve=cve_item.get('cve', '')).first()
            if item.get('MD5') != cve_object.MD5:
                db.session.add(PurpleCVE(**item))
                db.session.commit()
        else:
            db.session.add(PurpleCVE(**item))
            # db.session.update(PurpleCVE(**item))
            db.session.commit()

    def RunUpdate(self):
        # 1. get file from ofer ssh to create list of cve's with corresponding cpe's
        # 2. update_nvd_files to get the latest files from nvd
        # 3. parse and bulid each cve with cpe using build_cve method
        # 4. get_kb_for_cve for each cve
        # 5. get_eol_data to get eol of products

        # # 1
        # self.get_ssh_file()
        #
        # # 2
        # self.update_nvd_files()
        #
        # # 3
        # cve = ''
        # self.build_cve()
        #
        # #4
        # cve = ''
        # self.get_kb_for_cve(cve)
        #
        # #5
        # self.get_eol_data()

        real_cve = []

        def worker(cve):
            if cve in self.nvd_vulnerabilities.keys():
                self.get_kb_for_cve(cve)
                self.build_cve(self.nvd_vulnerabilities.get(cve))

        location = '{0}/../Data/cve_to_product.json'.format(os.getcwd())
        print('Updating CVE to CPE')
        with open(location) as f:
            self.cpe_data = json.load(f)

        cve_list = list(self.cpe_data.keys())
        print('Updating NVD Files')
        self.update_nvd_files()
        print('Updating CVE !!')
        for cve in tqdm(cve_list, desc='Committed'):
            worker(cve)
        # self.get_cve_details_files()
        print('Updating EOL Data')
        self.get_eol_data()
        print('Updating Application Data')
        self.load_application_port()


# db.create_all()
Updater().RunUpdate()
#Updater().get_eol_data()
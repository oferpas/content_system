import json
import time
import hashlib
import requests
from App.config import db
from bs4 import BeautifulSoup
from datetime import datetime
from tqdm.contrib.concurrent import thread_map
from App.db import PurpleCVE, PurpleContentFiles


# Variables
classification = {}
categories = [
    'Sql Injection',
    'Gain privileges',
    'Http response splitting',
    'Execute Code',
    'Memory corruption',
    'Directory traversal',
    'CSRF',
    'Overflow',
    'Denial Of Service',
    'Bypass a restriction or similar',
    'Obtain Information',
    'Cross Site Scripting'
]


class CVEDetails:

    def __init__(self, cve):
        self.results = {}
        self.search_url = 'http://www.cvedetails.com/cve/{0}'.format(cve)
        self.cve = cve
        self.soup = self._parse_html()
        self.parse()

    def _parse_html(self):
        response = requests.get(self.search_url)
        if not response:
            return None
        cve_html = response.content
        soup = BeautifulSoup(cve_html, "html.parser")
        return soup if soup != '' else None

    def _parse_error(self):
        return True if self.soup is not None and 'Unknown CVE ID' in self.soup.text else False

    def _parse_summery(self):
        return self.soup.find('div', class_="cvedetailssummary").text.split("\n")[1].lstrip('\t')

    def _parse_publish_date(self):
        return self.soup.find('div', class_="cvedetailssummary").text.split("\n")[3].split("\t")[1].split(":")[1].lstrip(' ')

    def _parse_product_data(self):
        products = []
        for row in self.soup.find(id='vulnprodstable').findAll('tr')[::-1]:
            cols = row.findAll('td')
            if cols and 'No vulnerable product found' not in cols[0].text:
                products.append({
                    'softwareType': cols[1].text.strip(),
                    'vendor': cols[2].text.strip(),
                    'product': cols[3].text.strip(),
                    'version': cols[4].text.strip(),
                    'update': cols[5].text.strip(),
                    'edition': cols[6].text.strip(),
                    'language': cols[7].text.strip()
                })

        return products

    def _parse_cvss_data(self):

        cvss = {}

        def vulnerability_categories(data):
            vulnerabilities = []
            for category in categories:
                if category in data:
                    vulnerabilities.append(category)
            return vulnerabilities

        for row in self.soup.find(id='cvssscorestable').findAll('tr'):

            if row.find('td').text.startswith('\n'):
                if row.find('td').parent.contents[1].text == 'Vulnerability Type(s)':
                    cvss[row.find('td').parent.contents[1].text] = vulnerability_categories(row.find('td').text.split('\n')[1])
                else:
                    cvss[row.find('td').parent.contents[1].text] = row.find('td').text.split('\n')[1]
            else:
                if row.find('td').parent.contents[1].text == 'Vulnerability Type(s)':
                    cvss[row.find('td').parent.contents[1].text] = vulnerability_categories(row.find('td').text.split('\n')[0])
                else:
                    cvss[row.find('td').parent.contents[1].text] = row.find('td').text.split('\n')[0]

        return cvss

    def parse(self):

        if self._parse_error():
            return self.results

        # time.sleep(0.1)
        self.results = {
            'cve': self.cve,
            'summery': self._parse_summery(),
            'publish_date': self._parse_publish_date(),
            'vulnerable_products': self._parse_product_data(),
            'cvss': self._parse_cvss_data()
        }

    def results(self):
        return self.results


def workerThread(cve):
    results = CVEDetails(cve=cve).results
    if results:
        # print('{0} Classification: {1}'.format(cve, results.get('cvss', {}).get('Vulnerability Type(s)', [])))
        classification[cve] = results
        # classification.add(results.get('cvss', {}).get('Vulnerability Type(s)', ''))

    entry = {
        'filename': 'cve-details',
        'file_status': 'Updated',
        'http_status': 200,
        'file_content': json.dumps(classification),
        'MD5': hashlib.md5(json.dumps(classification).encode('utf-8')).hexdigest(),
        'date': datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    }

    db.session.add(PurpleContentFiles(**entry))
    db.session.commit()


# cve_list = [i.cve for i in PurpleCVE.query.all()]
#
# # with open('/Users/mrunix/PycharmProjects/CveBasedContentSystem/App/loaders/allnvd.json') as f:
# #     cve_list = json.load(f).get('CVES')
# t = time.process_time()
# thread_map(workerThread, cve_list, max_workers=20)
# elapsed_time = time.process_time() - t
# print('Total Run Time: {0}'.format(elapsed_time))


# print(classification)

# CVEDetails(cve='CVE-2020-1328').results
import os
import json
from App.config import db
from App.db import PurpleContentFiles


class NVDLoad(object):
    def __init__(self, filename):
        self.filename = filename
        self.data = self._parse_json_objects()
        self.mapped_cves = {}
        self.format_cves()

    def _parse_json_objects(self):
        if PurpleContentFiles.query.filter_by(filename=self.filename).count() > 0:
            file_details = db.session.query(PurpleContentFiles).filter_by(filename=self.filename).one()
            return json.loads(file_details.file_content)

    def _extract_cve(self, cve_item):
        return cve_item.get('cve', {}).get('CVE_data_meta', {}).get('ID')

    def _extract_cwe(self, cve_item):
        cwes_data = set([])
        cwes_prob_data = cve_item.get('cve', {}).get('problemtype', {}).get('problemtype_data', [])
        for cwe_prob in cwes_prob_data:
            for cwe in cwe_prob.get('description', []):
                cwe_value = cwe.get('value')
                if cwe_value:
                    cwes_data.add(cwe_value)
        return ','.join(cwes_data)

    def _extract_references(self, cve_item):
        references_data = set([])
        reference_prob_data = cve_item.get('cve', {}).get('references', {}).get('reference_data', [])
        for reference_prob in reference_prob_data:
            references_data.add(reference_prob.get('url'))
        return ','.join(references_data)

    def _extract_description(self, cve_item):
        description_data = set([])
        description_prob_data = cve_item.get('cve', {}).get('description', {}).get('description_data', [])
        for description_prob in description_prob_data:
            description_data.add(description_prob.get('value'))
        return ','.join(description_data)

    def _extract_configurations(self, cve_item):
        configurations_data = {}
        description_prob_data = cve_item.get('configurations', {}).get('nodes', [])
        for configuration_prob in description_prob_data:
            operator = configuration_prob.get('operator', '')
            cpe_list = []
            if 'children' in configuration_prob.keys():
                childrens = []
                for children in configuration_prob.get('children'):
                    children_operator = children.get('operator')
                    children_cpe = []
                    for cpe in children.get('cpe_match', ''):
                        children_cpe.append(cpe.get('cpe23Uri', ''))
                    childrens.append({
                        children_operator: children_cpe
                    })
                configurations_data['childrens'] = childrens

            for cpe in configuration_prob.get('cpe_match', ''):
                if cpe.get('vulnerable') is True:
                    cpe_list.append(cpe.get('cpe23Uri', ''))
            configurations_data[operator] = cpe_list
        return configurations_data

    def _extract_metricV2(self, cve_item):
        return cve_item.get('impact', {}).get('baseMetricV2', {})

    def _extract_metricV3(self, cve_item):
        return cve_item.get('impact', {}).get('baseMetricV3', {})

    def _extract_last_modified_date(self, cve_item):
        return cve_item.get('lastModifiedDate', '')

    def _extract_published_date(self, cve_item):
        return cve_item.get('publishedDate', '')

    def _format_cve_item(self, cve_item):
        return {
                'cve': self._extract_cve(cve_item),
                'description': self._extract_description(cve_item),
                'configurations': self._extract_configurations(cve_item),
                'published_date': self._extract_published_date(cve_item),
                'last_modified_date': self._extract_last_modified_date(cve_item),
                'references': self._extract_references(cve_item),
                'cwe': self._extract_cwe(cve_item),
                'metricV2': self._extract_metricV2(cve_item),
                'metricV3': self._extract_metricV3(cve_item),
        }

    # @property
    def format_cves(self):
        self.mapped_cves = {
            self._extract_cve(cve_item): self._format_cve_item(cve_item) for cve_item in self.data.get('CVE_Items', [])
        }
        # return self.mapped_cves

    @property
    def cves(self):
        return self.mapped_cves

# CVE_Items ={}
#
# # for filename in sorted(os.listdir('Data')):
# nvd_test = NVDtest('Data/{0}'.format('nvdcve-1.1-2020.json'))
# CVE_Items.update(nvd_test.cves)
#
#
# print(CVE_Items)
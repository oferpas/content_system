import os
import csv
import json
import gzip
import hashlib
import paramiko
import requests
from tqdm import tqdm
from App.config import db
from bs4 import BeautifulSoup
from App.config import SSHConfig
from datetime import date, datetime
from App.Update.NvdLoader import NVDLoad
from multiprocessing.pool import ThreadPool
from App.Update.UpdateCVEDetails import CVEDetails
from App.db import PurpleCVE, PurpleContentFiles, PurpleEOL, PurpleCveToKb, PurpleApplication
# from App.Classifier.VulnClassifier import AutomatedVulnerabilityClassifier
from tqdm.contrib.concurrent import thread_map


class CVEDetailsUpdate(object):

    def __init__(self):
        self.results = {}
        self.thread_number = 2
        self.search_url = 'http://www.cvedetails.com/cve/{}'

        self.categories = [
            'Sql Injection', 'Gain privileges', 'Http response splitting',
            'Execute Code', 'Memory corruption', 'Directory traversal',
            'CSRF', 'Overflow', 'Denial Of Service',
            'Bypass a restriction or similar', 'Obtain Information', 'Cross Site Scripting'
        ]
        cve_list = [i.cve for i in PurpleCVE.query.all()]
        ThreadPool(self.thread_number).map(self.worker, cve_list)
        self._updater()

    def _updater(self):

        entry = {
            'filename': 'cve-details',
            'file_status': 'Updated',
            'http_status': 200,
            'file_content': json.dumps(self.results),
            'MD5': hashlib.md5(json.dumps(self.results).encode('utf-8')).hexdigest(),
            'date': datetime.now().strftime("%d/%m/%Y %H:%M:%S")
        }

        db.session.add(PurpleContentFiles(**entry))
        db.session.commit()

    def _parse_html(self, cve):
        response = requests.get(self.search_url.format(cve))
        if not response:
            return None
        cve_html = response.content
        soup = BeautifulSoup(cve_html, "html.parser")
        return soup if soup != '' else None

    def _parse_error(self, soup):
        return True if soup is not None and 'Unknown CVE ID' in soup.text else False

    def _parse_summery(self, soup):
        return soup.find('div', class_="cvedetailssummary").text.split("\n")[1].lstrip('\t')

    def _parse_publish_date(self, soup):
        return soup.find('div', class_="cvedetailssummary").text.split("\n")[3].split("\t")[1].split(":")[1].lstrip(' ')

    def _parse_product_data(self, soup):
        products = []
        for row in soup.find(id='vulnprodstable').findAll('tr')[::-1]:
            cols = row.findAll('td')
            if cols and 'No vulnerable product found' not in cols[0].text:
                products.append({
                    'softwareType': cols[1].text.strip(),
                    'vendor': cols[2].text.strip(),
                    'product': cols[3].text.strip(),
                    'version': cols[4].text.strip(),
                    'update': cols[5].text.strip(),
                    'edition': cols[6].text.strip(),
                    'language': cols[7].text.strip()
                })

        return products

    def _parse_cvss_data(self, soup):

        cvss = {}

        def vulnerability_categories(data):
            vulnerabilities = []
            for category in self.categories:
                if category in data:
                    vulnerabilities.append(category)
            return vulnerabilities

        for row in soup.find(id='cvssscorestable').findAll('tr'):

            if row.find('td').text.startswith('\n'):
                if row.find('td').parent.contents[1].text == 'Vulnerability Type(s)':
                    cvss[row.find('td').parent.contents[1].text] = vulnerability_categories(
                        row.find('td').text.split('\n')[1])
                else:
                    cvss[row.find('td').parent.contents[1].text] = row.find('td').text.split('\n')[1]
            else:
                if row.find('td').parent.contents[1].text == 'Vulnerability Type(s)':
                    cvss[row.find('td').parent.contents[1].text] = vulnerability_categories(
                        row.find('td').text.split('\n')[0])
                else:
                    cvss[row.find('td').parent.contents[1].text] = row.find('td').text.split('\n')[0]

        return cvss

    def worker(self, cve):

        print('Working on CVE: {}\n'.format(cve))

        soup = self._parse_html(cve)
        if self._parse_error(soup):
            return self.results

        # time.sleep(0.1)
        result = {
            'cve': cve,
            'summery': self._parse_summery(soup),
            'publish_date': self._parse_publish_date(soup),
            'vulnerable_products': self._parse_product_data(soup),
            'cvss': self._parse_cvss_data(soup)
        }

        self.results[cve] = result


class EOLUpdate(object):

    def __init__(self):

        self.eol_url = 'https://endoflife.date/'
        self.eol_url_json = 'https://endoflife.date/api/{0}.json'
        self.keys = ['cycle', 'cycleShortHand', 'lts', 'release', 'support', 'eol', 'latest', 'link']
        self.UpdateEOL()

    def UpdateEOL(self):

        def parse_product_json(json):
            data = []
            for item in json:
                tmp = {}
                for key in self.keys:
                    if key not in item.keys():
                        continue
                    tmp[key] = item.get(key)
                data.append(tmp)
            return data

        products = {}

        page = requests.get(self.eol_url)

        soup = BeautifulSoup(page.content, "html.parser")
        results = soup.find("ul", class_="nav-list")
        for result in results:
            products[result.text] = result.find(href=True)['href'].lstrip('/')

        for product in products:
            json_data = json.loads(requests.get(self.eol_url_json.format(products.get(product))).text)
            items_list = parse_product_json(json_data)

            for item in items_list:

                name = item.get('cycle') if product in item.get('cycle') else "{} {}".format(product, item.get('cycle'))
                md5_signature = {'name': name,
                                 'release': item.get('release'),
                                 'support': item.get('support'),
                                 'eol': item.get('eol'),
                                 }

                if PurpleEOL.query.filter_by(name=name).count() <= 0:
                    entry = {
                        'name': name,
                        'cycle': item.get('cycle'),
                        'cycleShortHand': item.get('cycleShortHand'),
                        'lts': item.get('lts'),
                        'release': item.get('release'),
                        'support': item.get('support'),
                        'eol': item.get('eol'),
                        'latest': item.get('latest'),
                        'link': item.get('link'),
                        'MD5': hashlib.md5(json.dumps(md5_signature).encode("utf-8")).hexdigest()
                    }
                    db.session.add(PurpleEOL(**entry))
                    db.session.commit()

                else:
                    record = db.session.query(PurpleEOL).filter_by(name=name).one()
                    if record.MD5 != hashlib.md5(json.dumps(md5_signature).encode("utf-8")).hexdigest():
                        record.name = name
                        record.cycle = item.get('cycle')
                        record.cycleShortHand = item.get('cycleShortHand'),
                        record.lts = item.get('lts')
                        record.release = item.get('release')
                        record.support = item.get('support')
                        record.eol = item.get('eol')
                        record.latest = item.get('latest')
                        record.link = item.get('link')
                        record.MD5 = hashlib.md5(json.dumps(md5_signature).encode("utf-8")).hexdigest()
                        db.session.commit()


class NVDDefinitionUpdate(object):

    def __init__(self):
        self.nvd_files_names = [2002, date.today().year, 'recent']
        self.nvd_json_url = 'https://nvd.nist.gov/feeds/json/cve/1.1/nvdcve-1.1-{0}.json.gz'
        self.UpdateNVD()

    def download(self, url):
        request = requests.get(url)
        return request if request is not None and request.status_code == 200 else None

    def UpdateNVD(self):
        item_list = [self.nvd_files_names[2]]
        item_list.extend(list(map(str, range(self.nvd_files_names[0], self.nvd_files_names[1]+1))))
        item_list.sort()
        for item in item_list:
            filename = self.nvd_json_url.format(item).rsplit('/')[-1].strip('.gz')
            file_request = self.download(self.nvd_json_url.format(item))
            if file_request is None:
                continue

            if PurpleContentFiles.query.filter_by(filename=filename).count() <= 0:
                if file_request is None:
                    entry = {
                        'filename': filename,
                        'file_status': 'Failed',
                        'http_status': file_request.status_code,
                        'date': datetime.now().strftime("%d/%m/%Y %H:%M:%S")
                    }
                else:
                    entry = {
                        'filename': filename,
                        'file_status': 'Updated',
                        'http_status': file_request.status_code,
                        'file_content': json.dumps(json.loads(gzip.decompress(file_request.content))),
                        'MD5': hashlib.md5(
                            json.dumps(json.loads(gzip.decompress(file_request.content))).encode('utf-8')).hexdigest(),
                        'date': datetime.now().strftime("%d/%m/%Y %H:%M:%S")

                    }
                db.session.add(PurpleContentFiles(**entry))
                db.session.commit()

            else:
                file_details = db.session.query(PurpleContentFiles).filter_by(filename=filename).one()

                if file_request is None:
                    file_details.file_status = 'Failed'
                    file_details.date = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
                else:
                    downloaded_md5 = hashlib.md5(
                        json.dumps(json.loads(gzip.decompress(file_request.content))).encode('utf-8')).hexdigest()
                    if file_details.MD5 != downloaded_md5:
                        file_details.file_status = 'Updated'
                        file_details.http_status = file_request.status_code
                        file_details.file_content = json.dumps(json.loads(gzip.decompress(file_request.content)))
                        file_details.MD5 = hashlib.md5(
                            json.dumps(json.loads(gzip.decompress(file_request.content))).encode('utf-8')).hexdigest()
                        file_details.date = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
                    else:
                        file_details.date = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
                    db.session.commit()

        # self.nvd_files = [item.filename for item in PurpleContentFiles.query.all() if 'nvdcve' in item.filename]
        # [self.nvd_vulnerabilities.update(NVDLoad('{0}'.format(filename)).cves) for filename in self.nvd_files]


class ApplicationDefinitionUpdate(object):

    def __init__(self):
        self.application_document = 'Applications_to_service1.csv'

    def UpdateApplication(self):
        data = []
        location = '{0}/App/Data/{1}'.format(os.getcwd(), self.application_document)
        with open(location, mode='r') as csv_file:
            csv_reader = csv.DictReader(csv_file)
            for row in csv_reader:
                print('Adding Application {0} To DB'.format(row.get('Application')))
                Application_Name = row.get('Application')
                Application_Service = row.get('Service') if '-' not in row.get('Service') else ''
                TCP = row.get('TCP Default ports')
                UDP = row.get('UDP Default ports')

                item = {
                    'Application_Name': Application_Name,
                    'Application_Service': Application_Service,
                    'TCP': TCP,
                    'UDP': UDP
                }
                data.append(item)
                db.session.add(PurpleApplication(**item))
                db.session.commit()


class CVEDefinitionUpdate(object):

    def __init__(self):
        self.cpe_data = {}
        self.thread_number = 10
        self.nvd_vulnerabilities = {}
        # self.cpe_file_location = '{0}/App/Data/cve_to_product.json'.format(os.getcwd())
        self.cpe_file_location = '{0}/../Data/cve_to_product.json'.format(os.getcwd())
        self.read_cpe_file()

        self.cve_list = list(self.cpe_data.keys())
        self.nvd_files = [item.filename for item in PurpleContentFiles.query.all() if 'nvdcve' in item.filename]
        [self.nvd_vulnerabilities.update(NVDLoad('{0}'.format(filename)).cves) for filename in self.nvd_files]

        ThreadPool(self.thread_number).map(self.worker, self.cve_list)

    def read_cpe_file(self):
        with open(self.cpe_file_location) as f:
            self.cpe_data = json.load(f)

    def worker(self, cve):

        cve_item = self.nvd_vulnerabilities.get(cve)
        if cve_item is None:
            return

        item = {
            'cve': cve_item.get('cve', ''),
            'cwe': cve_item.get('cwe', ''),
            'references': cve_item.get('references', ''),
            'description': cve_item.get('description', ''),
            'published_date': cve_item.get('published_date', ''),
            'last_modified_date': cve_item.get('last_modified_date', '')
        }

        cpe_item = self.cpe_data.get(item.get('cve'))
        products = cpe_item.get('cpe').get('products')
        platforms = cpe_item.get('cpe').get('platforms')

        products_cpe = set([])
        if products:
            for product in products:
                for product_cpe in products.get(product):
                    products_cpe.add(product_cpe)

        platforms_cpe = set([])
        if platforms:
            for platform in platforms:
                for platform_cpe in platforms.get(platform):
                    platforms_cpe.add(platform_cpe)

        metricV2, metricV3 = {}, {}
        item['title'] = cpe_item.get('title')
        item['Product_CPE'] = ','.join(products_cpe)
        item['Platform_CPE'] = ','.join(platforms_cpe)
        item['From_Patch'] = cpe_item.get('from_patch')

        if cve_item.get('metricV2'):
            metricV2 = {
                'metricv2_cvss_version': cve_item.get('metricV2', '').get('cvssV2', '').get('version', ''),
                'metricv2_cvss_vectorString': cve_item.get('metricV2', '').get('cvssV2', '').get('vectorString', ''),
                'metricv2_cvss_accessVector': cve_item.get('metricV2', '').get('cvssV2', '').get('accessVector', ''),
                'metricv2_cvss_accessComplexity': cve_item.get('metricV2', '').get('cvssV2', '').get('accessComplexity',
                                                                                                     ''),
                'metricv2_cvss_authentication': cve_item.get('metricV2', '').get('cvssV2', '').get('authentication',
                                                                                                   ''),
                'metricv2_cvss_confidentialityImpact': cve_item.get('metricV2', '').get('cvssV2', '').get(
                    'confidentialityImpact', ''),
                'metricv2_cvss_integrityImpact': cve_item.get('metricV2', '').get('cvssV2', '').get('integrityImpact',
                                                                                                    ''),
                'metricv2_cvss_availabilityImpact': cve_item.get('metricV2', '').get('cvssV2', '').get(
                    'availabilityImpact', ''),
                'metricv2_cvss_baseScore': cve_item.get('metricV2', '').get('cvssV2', '').get('baseScore', 0.0),

                'metricv2_severity': cve_item.get('metricV2', '').get('severity', ''),
                'metricv2_exploitability_score': cve_item.get('metricV2', '').get('exploitability_score', 0.0),
                'metricv2_impact_score': cve_item.get('metricV2', '').get('impact_score', 0.0),
                'metricv2_acInsufInfo': cve_item.get('metricV2', '').get('acInsufInfo', False),
                'metricv2_obtainAllPrivilege': cve_item.get('metricV2', '').get('obtainAllPrivilege', False),
                'metricv2_obtainUserPrivilege': cve_item.get('metricV2', '').get('obtainUserPrivilege', False),
                'metricv2_obtainOtherPrivilege': cve_item.get('metricV2', '').get('obtainOtherPrivilege', False),
                'metricv2_userInteractionRequired': cve_item.get('metricV2', '').get('userInteractionRequired', False),
            }
            item.update(metricV2)

        if cve_item.get('metricV3'):
            metricV3 = {
                'metricv3_cvss_version': cve_item.get('metricV3', '').get('cvssV3', '').get('version', ''),
                'metricv3_cvss_vectorString': cve_item.get('metricV3', '').get('cvssV3', '').get('vectorString', ''),
                'metricv3_cvss_attackVector': cve_item.get('metricV3', '').get('cvssV3', '').get('attackVector', ''),
                'metricv3_cvss_attackComplexity': cve_item.get('metricV3', '').get('cvssV3', '').get('attackComplexity',
                                                                                                     ''),
                'metricv3_cvss_privilegesRequired': cve_item.get('metricV3', '').get('cvssV3', '').get(
                    'privilegesRequired', ''),
                'metricv3_cvss_userInteraction': cve_item.get('metricV3', '').get('cvssV3', '').get('userInteraction',
                                                                                                    ''),
                'metricv3_cvss_scope': cve_item.get('metricV3', '').get('cvssV3', '').get('scope', ''),
                'metricv3_cvss_confidentialityImpact': cve_item.get('metricV3', '').get('cvssV3', '').get(
                    'confidentialityImpact', ''),
                'metricv3_cvss_integrityImpact': cve_item.get('metricV3', '').get('cvssV3', '').get('integrityImpact',
                                                                                                    ''),
                'metricv3_cvss_availabilityImpact': cve_item.get('metricV3', '').get('availabilityImpact', ''),
                'metricv3_cvss_baseScore': cve_item.get('metricV3', '').get('baseScore', 0.0),
                'metricv3_cvss_baseSeverity': cve_item.get('metricV3', '').get('baseSeverity', ''),
                'metricv3_exploitability_score': cve_item.get('metricV3', '').get('exploitability_score', 0.0),
                'metricv3_impact_score': cve_item.get('metricV3', '').get('impact_score', 0.0),

            }
            item.update(metricV3)

            md5_signature = {'description': cve_item.get('description', ''),
                             'published_date': cve_item.get('published_date', '')}
            if metricV2:
                md5_signature.update(metricV2)
            if metricV3:
                md5_signature.update(metricV3)
            item['MD5'] = hashlib.md5(json.dumps(md5_signature).encode("utf-8")).hexdigest()

        if PurpleCVE.query.filter_by(cve=cve_item.get('cve', '')).count() > 0:
            cve_object = PurpleCVE.query.filter_by(cve=cve_item.get('cve', '')).first()
            if item.get('MD5') != cve_object.MD5:
                cve_object.cwe = item.get('cwe', None)
                cve_object.title = item.get('title', None)

                cve_object.Product_CPE = item.get('Product_CPE', None)
                cve_object.Platform_CPE = item.get('Platform_CPE', None)
                cve_object.From_Patch = item.get('From_Patch', None)

                cve_object.references = item.get('references', None)
                cve_object.description = item.get('description', None)
                cve_object.published_date = item.get('published_date', None)
                cve_object.last_modified_date = item.get('last_modified_date', None)
                cve_object.metricv2_cvss_version = item.get('metricv2_cvss_version', None)
                cve_object.metricv2_cvss_vectorString = item.get('metricv2_cvss_vectorString', None)
                cve_object.metricv2_cvss_accessVector = item.get('metricv2_cvss_accessVector', None)
                cve_object.metricv2_cvss_accessComplexity = item.get('metricv2_cvss_accessComplexity', None)
                cve_object.metricv2_cvss_authentication = item.get('metricv2_cvss_authentication', None)
                cve_object.metricv2_cvss_confidentialityImpact = item.get('metricv2_cvss_confidentialityImpact', None)
                cve_object.metricv2_cvss_integrityImpact = item.get('metricv2_cvss_integrityImpact', None)
                cve_object.metricv2_cvss_availabilityImpact = item.get('metricv2_cvss_availabilityImpact', None)
                cve_object.metricv2_cvss_baseScore = item.get('metricv2_cvss_baseScore', None)

                cve_object.metricv2_severity = item.get('metricv2_severity', None)
                cve_object.metricv2_exploitability_score = item.get('metricv2_exploitability_score', None)
                cve_object.metricv2_impact_score = item.get('metricv2_impact_score', None)
                cve_object.metricv2_acInsufInfo = item.get('metricv2_acInsufInfo', None)
                cve_object.metricv2_obtainAllPrivilege = item.get('metricv2_obtainAllPrivilege', None)
                cve_object.metricv2_obtainUserPrivilege = item.get('metricv2_obtainUserPrivilege', None)
                cve_object.metricv2_obtainOtherPrivilege = item.get('metricv2_obtainOtherPrivilege', None)
                cve_object.metricv2_userInteractionRequired = item.get('metricv2_userInteractionRequired', None)

                cve_object.metricv3_cvss_version = item.get('metricv3_cvss_version', None)
                cve_object.metricv3_cvss_vectorString = item.get('metricv3_cvss_vectorString', None)
                cve_object.metricv3_cvss_attackVector = item.get('metricv3_cvss_attackVector', None)
                cve_object.metricv3_cvss_attackComplexity = item.get('metricv3_cvss_attackComplexity', None)
                cve_object.metricv3_cvss_privilegesRequired = item.get('metricv3_cvss_privilegesRequired', None)
                cve_object.metricv3_cvss_userInteraction = item.get('metricv3_cvss_userInteraction', None)
                cve_object.metricv3_cvss_scope = item.get('metricv3_cvss_scope', None)
                cve_object.metricv3_cvss_confidentialityImpact = item.get('metricv3_cvss_confidentialityImpact', None)
                cve_object.metricv3_cvss_integrityImpact = item.get('metricv3_cvss_integrityImpact', None)
                cve_object.metricv3_cvss_availabilityImpact = item.get('metricv3_cvss_availabilityImpact', None)
                cve_object.metricv3_cvss_baseScore = item.get('metricv3_cvss_baseScore', None)
                cve_object.metricv3_cvss_baseSeverity = item.get('metricv3_cvss_baseSeverity', None)
                cve_object.MD5 = hashlib.md5(json.dumps(md5_signature).encode("utf-8")).hexdigest()

                # db.session.add(PurpleCVE(**item))
                db.session.commit()
        else:
            db.session.add(PurpleCVE(**item))
            db.session.commit()


class KBDefinitionUpdate(object):

    def __init__(self):

        self.thread_number = 10
        self.cve_list = [i.cve for i in PurpleCVE.query.all()]
        self.kb_api_url = 'http://data.netformx.com:5555/api/cve/CveAllProducts?vendor=Microsoft&cve={0}'
        ThreadPool(self.thread_number).map(self.worker, self.cve_list)

    def download(self, url):
        request = requests.get(url)
        return request if request is not None and request.status_code == 200 else None

    def worker(self, cve):

        items = []
        request = self.download(self.kb_api_url.format(cve))
        if request is None:
            return

        data = json.loads(request.text())
        if not data:
            return

        cve = data[0].get('Cve')
        description = data[0].get('Notes').get('Description')

        if PurpleCveToKb.query.filter_by(cve=cve).count() <= 0:
            for product in data[0].get('Products'):
                item = {}
                item['cve'] = cve
                item['description'] = description
                item['ExploitStatus'] = product.get('ExploitStatus')
                item['FullProductName'] = product.get('FullProductName')
                item['RemediationType'] = product.get('RemediationType')
                item['RestartRequired'] = product.get('RestartRequired')
                item['ProductId'] = product.get('ProductId')
                item['Supercedence'] = product.get('Supercedence')
                item['UrlBinaries'] = product.get('UrlBinaries')
                item['UrlFix'] = product.get('UrlFix')
                item['VendorFix'] = product.get('VendorFix')
                items.append(item)

            for entry in items:
                db.session.add(PurpleCveToKb(**entry))
            db.session.commit()


#data = CVEDetailsUpdate()
# data = NVDDefinitionUpdate()
data = CVEDefinitionUpdate()
# print(data)